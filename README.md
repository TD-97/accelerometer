# Accelerometer
The idea of this project was to write the software to enable communication with an Accelerometer, namely
the Analogue Devices ADXL362. The Accelerometer communicates over an SPI interface. 

We assume that the hardware provides two functions, SPIbyte and SPIselect. The details of which are covered
in the code.

The code is written in c. 