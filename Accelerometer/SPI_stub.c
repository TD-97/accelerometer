#include "typedef.h"
#include "ACL.h"
#include <stdio.h>

uint8 slaveNumberCur;
uint8 i=0;
uint8 accelData[8];
uint8 writeData;

/*	Stub of function which will do the following:
	Function to activate the slave select signal (active 
	low) for a specific slave device. Use argument ACL to 
	select the ADXL362. Use argument NONE to de-select all 
	slaves.
*/
void SPIselect(uint8 slaveNumber) {
	slaveNumberCur = slaveNumber;
}

/*	Stub of function which will do the following:
	Function to generate 8 clock pulses and transfer 8 bits
	on MOSI and MISO, MSB first. The argument is the byte to
	send on MOSI. The function will not return until the
	transfer is complete. The return value is the byte
	received on MISO.
*/
uint8 SPIbyte(uint8 byteTX) {
	if (slaveNumberCur == ACL) {
		//printf("i: %d\nData: %d\n",i, accelData[i]);

		i++;
		writeData = byteTX;
		//printf("i now: %d\n", i);
		return accelData[i-1];
	}
	else {
		return 0;
	}
}

void setAccelData(uint8 data[8]) {
	uint8 k=2;
	while (k < 8) {
		accelData[k] = data[k];
		k++;
	}
	return;
}

uint8 getAccelData(uint8 j) {
	return accelData[j];
}

uint8 getWriteData() {
	return writeData;
}

void resetCounter() {
	i = 0;
	return;
}