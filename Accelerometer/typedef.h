#ifndef TYPEDEF_HEADER_FILE
	#define TYPEDEF_HEADER_FILE
	// define some useful variable types
	typedef unsigned char uint8;
	typedef unsigned short int uint16;
	typedef signed short int int16;
	typedef unsigned long int uint32;
#endif
