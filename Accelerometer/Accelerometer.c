/* Example program for Accelerometer Applications 
	assignment. You need to include this file, SPImaster.h 
	and typedefs.h in your project.*/

#include <stdio.h>
#include <stdlib.h>
#include "typedefs.h"   // type definitions
#include "SPImaster.h"  // functions from the hardware team

#define NULL 0x0		// define null byte

// SPI commands used to communicate with the ADXL362
// standard commands are in form <CMD><REG ADDR><DATA>
#define W_REG_CMD 0x0A	// write register command
#define R_REG_CMD 0x0B	// read register command
#define R_FIFO_CMD 0x0D	// read fifo command

// ADXL362 registers
#define X_DATA_L 0x0E

/*	Function to read from a register in the ADXL362.
	The argument is the number of the register to read.
	The return value is the value read from the register.
*/
uint8 ADXLregRead(uint8 regNumber) {
	uint8 read_byte;			// byte to be returned

	SPIselect(ACL);				// select the accelerometer

	SPIbyte(R_REG_CMD);			// send read cmd
	SPIbyte(regNumber);			// send reg address to read
	read_byte = SPIbyte(NULL);	// read the response

	SPIselect(NONE);			// deselect the ADXL362

	return read_byte;			// return the byte
}

/*	Function to write to a register in the ADXL362.
	Arguments are the register number and the value to
	write.
*/
void ADXLregWrite(uint8 regNumber, uint8 value) {
	SPIselect(ACL);		// select the accelerometer
	SPIbyte(W_REG_CMD);	// send write cmd
	SPIbyte(regNumber);	// send reg address to write
	SPIbyte(value);		// write the value
	SPIselect(NONE);	// deselect the ADXL362
}

/*	Function to read 3 acceleration values from the ADXL362.
	The arguments are pointers to variables that hold the
	acceleration values. This function will change
	the values of those three variables.
*/
void ADXLgetAccel(int16* Xaccel, int16* Yaccel,
	int16* Zaccel) {
	uint8 i = 0;
	uint8 accel_xyz[6];

	SPIselect(ACL);		// select the accelerometer
	SPIbyte(R_REG_CMD);	// send read cmd
	SPIbyte(X_DATA_L);	// send reg address to read

	// read six consecutive addresses x,y,z upper and lower
	// bytes
	while (i < 6) {
		accel_xyz[i] = SPIbyte(NULL);
		i++;
	}
	SPIselect(NONE);

	// combine the upper and lower bytes to one signed
	// integer
	*Xaccel = (accel_xyz[1] << 8) | (accel_xyz[0]);
	*Yaccel = (accel_xyz[3] << 8) | (accel_xyz[2]);
	*Zaccel = (accel_xyz[5] << 8) | (accel_xyz[4]);
	return;
}

/* main function to call the other functions and test them*/
int main()
{
    uint8 regNum, regVal;         // register number and val
    int16 Xaccel, Yaccel, Zaccel; // acceleration components

    printf("\nReading registers\n");
    for (regNum = 0; regNum < 11; regNum++)
    {
        regVal = ADXLregRead( regNum );
        printf("Read from register %02x, got value %02x\n",\
			regNum, regVal);
    }

    printf("\nWriting registers\n");
    for (regNum = 0x20; regNum < 0x23; regNum++)
    {
        regVal += 22;   // some number
        ADXLregWrite(regNum, regVal);
        printf("Wrote to register %02x, value %02x\n",\
			regNum, regVal);
        regVal = ADXLregRead( regNum );
        printf("Read back value %02x\n", regVal);
   }

    printf("\nReading acceleration\n");
    ADXLgetAccel(&Xaccel, &Yaccel, &Zaccel);
    printf("Acceleration values X %d Y %d Z %d\n", Xaccel,\
		Yaccel, Zaccel);

    return 0;
}