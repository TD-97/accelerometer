#ifndef DE_TASK_1_H
#define DE_TASK_1_H

/*	Function to read from a register in the ADXL362.
	The argument is the number of the register to read.
	The return value is the value read from the register.
*/
uint8 ADXLregRead(uint8 regNumber);

/*	Function to write to a register in the ADXL362.
	Arguments are the register number and the value to
	write.
*/
void ADXLregWrite(uint8 regNumber, uint8 value);

/*	Function to read 3 acceleration values from the ADXL362.
	The arguments are pointers to variables that hold the
	acceleration values. This function will change
	the values of those three variables.
*/
void ADXLgetAccel(int16* Xaccel, int16* Yaccel,
	int16* Zaccel);
#endif
