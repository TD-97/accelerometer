#ifndef SPI_STUB_FILE
	#define SPI_STUB_FILE
	void SPIselect(uint8 slaveNumber);
	uint8 SPIbyte(uint8 byteTX);
	void setAccelData(uint8 data[6]);
	void resetCounter();
	uint8 getWriteData();
	uint8 getAccelData(uint8 j);
#endif
