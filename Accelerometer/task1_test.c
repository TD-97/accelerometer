#include "typedef.h"
#include "SPI_stub.h"
#include "task1.h"
#include "ACL.h"
#include <stdio.h>

int main()
{
	uint8 i=2;
	uint8 k = 0;
	uint8 accelData[8];
	
	int16 accelVal[3] = { -5, 400, -200 };

	while (i < 8) {
		accelData[i] = accelVal[k] & 0xff;
		printf("Data %d: %d\n", i, accelData[i]);
		i++;
		accelData[i] = (accelVal[k] >> 8);
		printf("Data %d: %d\n", i, accelData[i]);
		i++;
		k++;
	}

	setAccelData(accelData);
	resetCounter();

	uint8 gotByte = ADXLregRead(ACL);

	uint8 expByte = accelData[2];

	if (gotByte != expByte) {
		printf("FAIL. Expected: %d. Got: %d.\n", expByte,\
			gotByte);
	}
	else {
		printf("SUCCESS. Expected: %d. Got: %d.\n", \
			expByte, gotByte);
	}

	expByte = 150;
	ADXLregWrite(ACL, expByte);
	gotByte = getWriteData();

	if (gotByte != expByte) {
		printf("FAIL. Expected: %d. Got: %d.\n", expByte, \
			gotByte);
	}
	else {
		printf("SUCCESS. Expected: %d. Got: %d.\n", \
			expByte, gotByte);
	}
	
	resetCounter();

	// check the 
	int16 Xaccel, Yaccel, Zaccel;
	ADXLgetAccel(&Xaccel, &Yaccel, &Zaccel);

	if (Xaccel != accelVal[0]) {
		printf("FAIL. Expected: %d. Got: %d.\n",\
			accelVal[0], Xaccel);
	}
	else {
		printf("SUCCESS. Expected: %d. Got: %d.\n", \
			accelVal[0], Xaccel);
	}

	if (Yaccel != accelVal[1]) {
		printf("FAIL. Expected: %d. Got: %d.\n", \
			accelVal[1], Yaccel);
	}
	else {
		printf("SUCCESS. Expected: %d. Got: %d.\n", \
			accelVal[1], Yaccel);
	}

	if (Zaccel != accelVal[2]) {
		printf("FAIL. Expected: %d. Got: %d.\n",\
			accelVal[2], Zaccel);
	}
	else {
		printf("SUCCESS. Expected: %d. Got: %d.\n",\
			accelVal[2], Zaccel);
	}

	return;
}