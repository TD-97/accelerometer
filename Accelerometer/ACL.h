#ifndef ACL_H_FILE
	#define ACL_H_FILE

	#define ACL 2			// ADXL362 slave number
	#define NONE 0			// Deselect all slaves
	#define NULL 0x0		// define null byte

	// SPI commands used to communicate with the ADXL362
	// standard commands are in form <CMD><REG ADDR><DATA>
	#define W_REG_CMD 0x0A	// write register command
	#define R_REG_CMD 0x0B	// read register command
	#define R_FIFO_CMD 0x0D	// read fifo command

	// ADXL362 registers
	#define X_DATA_L 0x0E

#endif