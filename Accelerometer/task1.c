#include "typedefs.h"
//#include "SPI_stub.h"
#include "ACL.h"
#include <stdio.h>

/*	Function to read from a register in the ADXL362.
	The argument is the number of the register to read.
	The return value is the value read from the register.
*/
uint8 ADXLregRead(uint8 regNumber) {
	uint8 read_byte;			// byte to be returned

	SPIselect(ACL);				// select the accelerometer

	SPIbyte(R_REG_CMD);			// send read cmd
	SPIbyte(regNumber);			// send reg address to read
	read_byte = SPIbyte(NULL);	// read the response

	SPIselect(NONE);			// deselect the ADXL362

	return read_byte;			// return the byte
}

/*	Function to write to a register in the ADXL362.
	Arguments are the register number and the value to
	write.
*/
void ADXLregWrite(uint8 regNumber, uint8 value) {
	SPIselect(ACL);		// select the accelerometer
	SPIbyte(W_REG_CMD);	// send write cmd
	SPIbyte(regNumber);	// send reg address to write
	SPIbyte(value);		// write the value
	SPIselect(NONE);	// deselect the ADXL362
}

/*	Function to read 3 acceleration values from the ADXL362.
	The arguments are pointers to variables that hold the
	acceleration values. This function will change
	the values of those three variables.
*/
void ADXLgetAccel(int16* Xaccel, int16* Yaccel,
	int16* Zaccel) {
	uint8 i = 0;
	uint8 accel_xyz[6];

	SPIselect(ACL);		// select the accelerometer
	SPIbyte(R_REG_CMD);	// send read cmd
	SPIbyte(X_DATA_L);	// send reg address to read

	// read six consecutive addresses x,y,z upper and lower
	// bytes
	while (i < 6) {
		accel_xyz[i] = SPIbyte(NULL);
		i++;
	}
	SPIselect(NONE);

	// combine the upper and lower bytes to one signed
	// integer
	*Xaccel = (accel_xyz[1] << 8) | (accel_xyz[0]);
	*Yaccel = (accel_xyz[3] << 8) | (accel_xyz[2]);
	*Zaccel = (accel_xyz[5] << 8) | (accel_xyz[4]);
	return;
}